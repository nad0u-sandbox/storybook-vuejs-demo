# Storybook Vuejs Demo

## Note on Prettier
Prettier was complaining about single-quote in `*stories.js`, so it was disable temporarily.

## Project setup
```
npm install
```

### Storybook
```
npm run storybook
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
